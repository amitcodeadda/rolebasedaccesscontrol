const express = require('express');
const app = express();
const path = require("path");
const session = require('express-session');
const bodyParser = require('body-parser');
const flash = require("req-flash");
const mysql = require('mysql');

/* middleware
*********************************************************************************************************/
app.set('view engine', 'pug');
app.set("views", path.join(__dirname, "views"));

app.use(express.static(path.join(__dirname, "Public")));

app.use(session({
    secret: "secretSession"
}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(flash());


/*************************** database connection *******************************************/
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "employeemgmt"
});


/* Post and Get Requests
*********************************************************************************************************/
// Login and Log Out
app.get("/", function (req, res) {
    res.render("login", req.flash());
});
app.get("/logout", function (req, res) {
    req.session.destroy();
    res.redirect("/");
});

app.post('/api/find', (req, res) => {
    connection.query(`SELECT * FROM user`, (err, rows, fields) => {
        res.json(rows);
    });
});

app.post("/index", function (req, res) {
    if (req.body.login) {
        if (req.session.info && req.session.info.username) {
            res.redirect("/index");
        } else {
            let uname = req.body.uname;
            let pass = req.body.psw;

            connection.query(`SELECT a.*, b.* FROM user a left join role b on a.role_id = b.id WHERE username= "${uname}" AND password= "${pass}"`, (err, rows, fields) => {
                if (err) {
                    console.log("error : ", err);
                    return;
                } else if (rows.length == 0) {
                    req.flash("err", "No record were found in the database.")
                    res.redirect("/");
                    return;
                }

                let response = rows[0];
                req.session.info = {
                    firstName: response["first_name"],
                    middleName: response["middle_name"],
                    lastName: response["last_name"],
                    email: response["email"],
                    mobile: response["mobile"],
                    username: response["username"],
                    role: response["role_id"]
                }

                res.redirect("/index");
            });
        }
    }
});


// app.get("/index", function (req, res) {
//     if (req.session.info) {
//         res.render("index", req.session.info);
//     } else {
//         res.redirect("/");
//     }
// });

app.get('/add_user', function (req, res) {
    if (req.session.info) {
        var roles = [];
        connection.query(`SELECT * FROM role`, (err, rows, fields) => {
            rows.forEach(row => {
                roles.push(row.name)
            });

            res.render("addUser", {
                roles,
                firstName: req.session.info.firstName,
                role: req.session.info.role
            });
        });

    } else {
        res.redirect("/");
    }
});

app.get('/index', function (req, res) {
    if (req.session.info) {
        res.render('index',{
            firstName: req.session.info.firstName,
            role: req.session.info.role
        })
    }else{
        res.redirect("/");
    }
});


app.post('/add_user', function (req, res) {
    if (req.session.info) {
        let columnName = "";
        let values = "";
        let body = req.body;
        let userRole = "";

        for (let key in body) {
            if (body[key] == "" || body[key] == null) {
                // do nothing
            } else {
                if (key == "cnfpwd" || key == "role") {
                    if (key == "role") {
                        userRole = `"${body[key]}"`;
                    }

                } else {
                    columnName += `${key},`;
                    values += `'${body[key]}',`;
                }
            }
        }

        columnName = columnName.replace("pwd", "password");

        columnName = columnName.substring(0, columnName.length - 1);
        values = values.substring(0, values.length - 1);

        // let userRole = values.substring(values.lastIndexOf(",") + 1, values.length);


        connection.query(`SELECT * FROM role WHERE name=${userRole}`, (err, rows, fields) => {
            console.log("rows: ", rows);
            columnName += `,role_id`;
            values += `,'${rows[0].id}'`;

            console.log(`INSERT INTO user (${columnName}) VALUES (${values})`)
            connection.query(`INSERT INTO user (${columnName}) VALUES (${values})`, (err, rows, fields) => {
                if (err) {
                    console.log("insert query failed");
                    return;
                }
                res.redirect("/add_user");
                req.flash("successmsg", "user has been added successfully");
                console.log("user created successfully");
            });
        });

    }
});


app.get('/all_users', function (req, res) {
    if (req.session.info) {

        connection.query(`SELECT * FROM user`, (err, rows, fields) => {
            console.log("users: ", rows);
            res.render("user", {
                firstName: req.session.info.firstName,
                role: req.session.info.role,
                users : rows
            });
        });

        
    } else {
        res.redirect("/");
    }
});


app.listen("3000", () => {
    console.log("server started at port num 3000");
})